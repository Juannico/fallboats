using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Torreta : MonoBehaviour
{
    #region Variables

    // Las siguientes funciones solo son para propositos esteticos en el inspector de unity y no afectan la funci�n del c�digo en si

    // Space(espaciado en el inspector)
    // Header(titulo en el inspector)
    // Tooltip(texto al dejar el cursor sobre una variable en el inspector)
    
    [Space(10)]
    [Header("Atributos de la torreta")]

    [Tooltip("El objeto a ser disparado")]
    public GameObject municion;
    [Tooltip("El objeto al que disparara la torreta (usado para compensar la velocidad de bala)")]
    public GameObject guiaTorreta;
    [Tooltip("El objeto al que seguira la torreta")]
    public Transform jugador;
    [Tooltip("El objeto al que seguira la torreta")]
    public Transform[] bot;
    [Tooltip("El rango de vision de la torreta hacia el frente (eje Z)")]
    public float rangoVision;
    [Tooltip("El angulo de visi�n periferica de la torreta hacia adelante (eje Z)")]
    public float anguloVision;
    [Tooltip("El tiempo de retraso entre disparos de la torreta")]
    public float retraso;
    [Tooltip("La velocidad con la que el objeto a disparar sale disparado de la torreta")]
    public float velocidadBala;
    [Tooltip("El retraso para destruir un objeto")]
    public float destruir;
    [Tooltip("Sonido de disparo de la torreta")]
    public AudioClip disparar;
    // Booleano que coordina cuando dispara la torreta
    bool disparo = false;
    // Raycast que le da vision a la torreta
    Ray avistar;

    #endregion

    void Update()
    {
        Ver();
    }
    void FixedUpdate()
    {
        Disparo();
    }
    void Ver()
    {
        // Funci�n encargada de la visi�n de la torreta

        avistar = new Ray(transform.position, transform.forward * rangoVision);

    }
    void Disparo()
    {
        // Funci�n encargada del disparo de la torreta

        // Por defecto la torreta no dispara 

        disparo = false;

        // Si el jugador o un bot entra en el rango de vision de la torreta la torreta disparara e iniciara una corrutina

        if (Physics.Raycast(transform.position, transform.forward, rangoVision))
        {
            if (Vector3.Distance(transform.position, jugador.position) < rangoVision)
            {
                transform.LookAt(jugador);

                if (!disparo)
                {
                    StartCoroutine(Delay());
                }
            }

            if (Vector3.Distance(transform.position, bot[0].position) < rangoVision)
            {
                transform.LookAt(bot[0]);

                if (!disparo)
                {
                    StartCoroutine(Delay());
                }
            }
        }
    }

    IEnumerator Delay()
    {
        // Corrutina encargada de generar los proyectiles a disparar y a�adirles un retraso entre disparos

        // Cuando se ejecuta la corrutina la torreta:
        // Dispara
        // Se a�ade un retardo
        // Se crea un objeto(municion) en la posici�n y rotaci�n de un empty(guiaTorreta)
        // Reproduce el sonido del disparo
        // Se a�ade una fuerza hacia adelante lo que le da velocidad a la bala
        // La bala se destruye despues de un tiempo
        // No dispara

        disparo = true;
        yield return new WaitForSeconds(retraso); 
        GameObject generar = Instantiate(municion, guiaTorreta.transform.position, guiaTorreta.transform.rotation);
        AudioManager.instanciar.ReproducirSFX(disparar);
        generar.GetComponent<Rigidbody>().AddForce(generar.transform.forward * velocidadBala);
        Destroy(generar, destruir);
        disparo = false;
    }
}
