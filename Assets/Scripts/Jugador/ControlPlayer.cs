using UnityEngine.Playables;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using System.Collections;
using Cinemachine;
using UnityEngine.Audio;
public class ControlPlayer : MonoBehaviour
{
    #region Variables    

    // Las siguientes funciones solo son para propositos esteticos en el inspector de unity y no afectan la funci�n del c�digo en si

    // Space(espaciado en el inspector)
    // Header(titulo en el inspector)
    // Tooltip(texto al dejar el cursor sobre una variable en el inspector)

    #region Variables de movimiento del jugador

    [Space(10)]
    [Header("Variables de movimiento del jugador")]

    [Tooltip("joystick del jugador")]
    public Joystick stick;
    [Tooltip("El objeto donde se aplicar� el c�digo")]
    public GameObject jugador;
    [Tooltip("Velocidad del objeto hacia los lados (eje X)")]
    public float velocidadLat;
    [Tooltip("Velocidad m�xima del objeto hacia el frente (eje Z)")]
    public float velocidadMaxFront;
    [Tooltip("Velocidad del objeto hacia el frente (eje Z)")]
    public float velocidadFront;
    [Tooltip("Fuerza a�adida al objeto hacia arriba (eje Y)")]
    public float impulsoVert;
    [Tooltip("Fuerza a�adida al objeto hacia adelante (eje Z)")]
    public float impulsoFront;
    [Tooltip("Valor del radio del objeto suado como verificador del jugador cuando esta en el agua")]
    public float RadioAgua;
    [Tooltip("Objeto usado para verificar si el jugado esta en el agua")]
    public Transform agua;
    [Tooltip("capa de la pista")]
    public LayerMask pista;
    [Tooltip("sonidos del bote")]
    public AudioSource acelerar, chapoteo;
    [Tooltip("pitch minimo del sonido de acelerar del bote")]
    public float MinPitch = 0.1f;
    [Tooltip("pitch maximo del sonido de acelerar del bote")]
    public float MaxPitch = 2f;

    #endregion

    #region Variables de rotaci�n del jugador
    [Space(10)]
    [Header("Variables de rotaci�n del jugador")]

    [Tooltip("Rotaci�n del jugador")]
    public Transform RotacionPlayer;
    [Tooltip("Rotaci�n de referencia para el jugador")]
    public Transform RotacionRef;
    [Tooltip("Tiempo que dura la rotaci�n que restaura la rotaci�n del jugador")]
    public float TiempoRot;
    [Tooltip("Separaci�n entre el eje z del jugador y un eje z de referencia (si la separaci�n entre ambos ejes es mayor a la de esta variable se restaura la rotaci�n del jugador)")]
    public float diferenciaRot;
    [Tooltip("Cuanto rotara el jugador al moverse a los lados")]
    public float RotacionLat;
    #endregion

    #region Variables de reaparici�n del jugador

    [Space(10)]
    [Header("Variables de reaparici�n del jugador")]
    
    [Tooltip("Objeto donde reaparecera el jugador")]
    public GameObject PlayerRespawnRef;
    [Tooltip("El jugador al que el objeto perseguira")]
    public Transform ObjetivoRespawn;
    [Tooltip("Velocidad del objeto que seguira al jugador")]
    public float velocidadRespawn;
    [Tooltip("Separaci�n del objeto que seguira al jugador")]
    public Vector3 separacionRespawnDer;
    [Tooltip("Separaci�n del objeto que seguira al jugador")]
    public Vector3 separacionRespawnIzq;
    [Tooltip("Separaci�n del objeto que seguira al jugador")]
    public Vector3 separacionRespawn;

    #endregion

    #endregion


    void Start()
    {
        // Obtiene el audiosource asignado al jugador

        acelerar.GetComponent<AudioSource>();
    }

    void Update()
    {
        Mover();
    }

    void FixedUpdate()
    {
        Avance();
    }
    public void Mover()
    {
        // Funci�n encargada del movimiento lateral del jugador

        // Accede al eje horizontal del joystick, rota al jugador cuando el joystick se mueve horizontalmente y la velocidad del rigidbody del jugador la igual y le suma la velocidad lateral
        float x;
        x = stick.Horizontal + Input.GetAxis("Horizontal");
        jugador.transform.Rotate(0f, stick.Horizontal * RotacionLat * Time.deltaTime, 0f);
        jugador.GetComponent<Rigidbody>().velocity += new Vector3(x * velocidadLat, 0f, 0f);
        

        // Si hay una diferencia de 0.5 entre la rotaci�n del jugador y una rotaci�n de referencia restaura la rotaci�n del jugador en los tres ejes

        if (Vector3.Dot(RotacionRef.forward, jugador.transform.forward) > diferenciaRot)
        {
            print("eje z");
            print(jugador.transform.rotation);
            jugador.transform.rotation = Quaternion.Lerp(RotacionPlayer.rotation, RotacionRef.rotation, TiempoRot);
        }

        if (Vector3.Dot(RotacionRef.up, jugador.transform.up) > diferenciaRot)
        {
            print("eje y");
            print(jugador.transform.rotation);
            jugador.transform.rotation = Quaternion.Lerp(RotacionPlayer.rotation, RotacionRef.rotation, TiempoRot);
        }

        if (Vector3.Dot(RotacionRef.right, jugador.transform.right) > diferenciaRot)
        {
            print("eje x");
            print(jugador.transform.rotation);
            jugador.transform.rotation = Quaternion.Lerp(RotacionPlayer.rotation, RotacionRef.rotation, TiempoRot);
        }
    }

    void Avance()
    {
        // Funci�n encargada del movimiento frontal del jugador

        // Si el jugador esta en el agua tendra movimiento, si no esta en el agua el jugador no se podra mover

        // Checker para verificar si el jugador hace contacto con el agua de la pista por medio de un booleano

        bool estaenelagua;
        estaenelagua = Physics.CheckSphere(agua.position, RadioAgua, pista);

        // Si estaenelagua es verdadero:
        // A�ade una fuerza hacia adelante al jugador usando la velocidad frontal asignada
        // El volumen de chapoteo sube a uno
        // las variables de velocidad y rotacion del jugador se les asigna un valor

        // Si estaenelagua es falso entonces:
        // las variables de velocidad y rotacion del jugador seran de cero
        // El volumen de chapoteo baja a cero

        if (estaenelagua)
        {
            jugador.GetComponentInChildren<Rigidbody>().AddForce(jugador.transform.forward * velocidadFront);
            chapoteo.volume = 1;
            velocidadLat = 0.75f;
            RotacionLat = 30;
            velocidadFront = 25;
        }
        else
        {
            velocidadLat = 0f;
            RotacionLat = 0f;
            velocidadFront = 0f;
            chapoteo.volume = 0;
        }

        // Si la velocidad del jugador es mayor a 0 entonces obtenga la velocidad del jugador y suba el pitch del sonido del bote con respecto a la velocidad del jugador

        if (jugador.GetComponentInChildren<Rigidbody>().velocity.magnitude > 0f)
        {
            float referenciaVelocidad = jugador.GetComponentInChildren<Rigidbody>().velocity.magnitude;
            acelerar.pitch = Mathf.SmoothDamp(MinPitch, MaxPitch, ref referenciaVelocidad, 4f, velocidadMaxFront);
        }

        // Si la velocidad del jugador es mayor o igual a la velocidad m�xima del jugador iguala la velocidad del jugador con la velocidad m�xima del jugador

        if (jugador.GetComponentInChildren<Rigidbody>().velocity.magnitude >= velocidadMaxFront)
        {
            jugador.GetComponentInChildren<Rigidbody>().velocity = Vector3.ClampMagnitude(jugador.GetComponent<Rigidbody>().velocity, velocidadMaxFront);
        }
    }
}