using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using UnityEngine.Animations;

public class ColPlayer : MonoBehaviour
{
    #region Variables

    // Las siguientes funciones solo son para propositos esteticos en el inspector de unity y no afectan la funci�n del c�digo en si

    // Space(espaciado en el inspector)
    // Header(titulo en el inspector)
    // Tooltip(texto al dejar el cursor sobre una variable en el inspector)

    #region Variables del jugador

    [Space(10)]
    [Header("Variables del jugador")]

    [Tooltip("Script del jugador al que se va a acceder")]
    public ControlPlayer Activar;
    [Tooltip("Script del menu al que se va a acceder")]
    public MenuPlayer contarMonedas;
    [Tooltip("Objeto donde reaparecera el jugador")]
    public GameObject respawn;
    [Tooltip("Tiempo que tardara en reaparecer el jugador")]
    public float reaparicion;
    [Tooltip("El menu que aparecera en pantalla")]
    public GameObject menu;
    [Tooltip("La animacion de la camara a la que se cambiar� cuando el jugador llegue a la meta")]
    public Animator cinematica;
    [Tooltip("La camara donde se reproducira la aniamcion al llegar a la meta")]
    public Camera CamaraMeta;
    [Tooltip("Lista de objetos donde aparecera el confeti")]
    public GameObject[] spawnerConfeti;
    [Tooltip("efecto de confeti")]
    public GameObject confeti;
    [Tooltip("efectos de sonido")]
    public AudioClip impulso, madera, concreto, laser, explosion, coin;
    // booleano que coordina las colisones
    bool colision;

    #endregion

    #region Variables de la torreta

    [Space(10)]
    [Header("Variables de la torreta")]

    [Tooltip("Script de la torreta al que se va a acceder")]
    public Torreta Apagar;
    [Tooltip("Objeto que colisionara con el jugador y que es disparado por la torreta")]
    public GameObject Bullet;
    [Tooltip("Tiempo que tardara la torreta en reactivarse")]
    public int ApagarTorreta;

    #endregion

    #region Variables de cilindro gigante

    [Space(10)]
    [Header("Variables de cilindro gigante")]

    [Tooltip("El cilindro que aparecera")]
    public GameObject Cilindro;
    [Tooltip("Objeto de referencia donde reaparecera el cilindro")]
    public GameObject SpawnRefCilindro;
    [Tooltip("Objeto de referencia donde reaparecera el cilindro en la trmapa invertida")]
    public GameObject SpawnRefCilindroInv;
    [Tooltip("Tiempo que aparecera el cilindro")]
    public float duracion;
    [Tooltip("Impuslo hacia abajo del cilindro")]
    public float impulsoAbajo;

    #endregion

    #region Variables de seguimiento de c�mara

    [Space(10)]
    [Header("Variables de seguimiento de c�mara")]

    [Tooltip("La c�mara que seguira al jugador")]
    public Camera camaraJugador;
    [Tooltip("El jugador al que la c�mara perseguira")]
    public Transform objetivoCam;
    [Tooltip("Velocidad de la c�mara que seguira al jugador")]
    public float velocidadCam;
    [Tooltip("Separaci�n de la c�mara que seguira al jugador")]
    public Vector3 separacionCam;

    #endregion

    #endregion

    void Start()
    {
        // Animaciones y camaras desactivadas por defecto
        
        camaraJugador.enabled = true;
        cinematica.enabled = false;
        cinematica.SetBool("llegar", false);
        CamaraMeta.enabled = false;
    }

    void Update()
    {
        // Cuenta las moendas obtenidas y las convierte en texto que se muestra en pantalla
        
        contarMonedas.Monedas.text = contarMonedas.contadorMonedas.ToString();
    }

    void LateUpdate()
    {
        Seguir();
    }
    void OnCollisionEnter(UnityEngine.Collision choque)
    {
        // Colisiones que activan condiciones por medio de un tag

        // Si colisiona con un objeto con el tag madera reproduce un sonido, destruye el objeto con el que colisiona y le da algo de velocidad al jugador para que no se frene por completo
        // Si no colisiona con un objeto con el tag madera congela la rotacion en z del rigidbody al que esta accediendo

        if (choque.collider.tag == "madera")
        {
            AudioManager.instanciar.ReproducirSFX(madera);
            Destroy(choque.gameObject);
            Activar.jugador.GetComponent<Rigidbody>().AddForce(Vector3.forward * (Activar.velocidadMaxFront + Activar.impulsoFront));
        }
        else
        {
            Activar.jugador.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotationZ;
        }

        // Si colisiona con un objeto con el tag concreto reproduce un sonido, destruye el objeto con el que colisiona y le da algo de velocidad al jugador para que no se frene por completo

        if (choque.collider.tag == "concreto")
        {
            AudioManager.instanciar.ReproducirSFX(concreto);
            Destroy(choque.gameObject);
            Activar.jugador.GetComponent<Rigidbody>().AddForce(Vector3.forward * (Activar.velocidadMaxFront + Activar.impulsoFront));
        }

        // Si colisiona con un objeto con el tag barricada reproduce un sonido

        if (choque.collider.tag == "barricada")
        {
            AudioManager.instanciar.ReproducirSFX(concreto);
        }

        // Si colisiona con un objeto con el tag ParedDer desactiva el script al que esta accediendo,inicia las corrutinas,destruye el objeto Bullet y reproduce un sonido
        // Si no colisiona con un objeto con el tag ParedDer congela la rotacion en z del rigidbody al que esta accediendo

        if (choque.collider.tag == "ParedDer")
        {
            Activar.enabled = false;
            StartCoroutine(ReaparecerDer());
            StartCoroutine(Inmunidad());
            Destroy(Bullet);
            AudioManager.instanciar.ReproducirSFX(explosion);
        }
        else
        {
            Activar.jugador.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotationZ;
        }

        // Si colisiona con un objeto con el tag ParedIzq desactiva el script al que esta accediendo,inicia las corrutinas,destruye el objeto Bullet y reproduce un sonido
        // Si no colisiona con un objeto con el tag ParedIzq congela la rotacion en z del rigidbody al que esta accediendo

        if (choque.collider.tag == "ParedIzq")
        {
            Activar.enabled = false;
            StartCoroutine(ReaparecerIzq());
            StartCoroutine(Inmunidad());
            Destroy(Bullet);
            AudioManager.instanciar.ReproducirSFX(explosion);
        }
        else
        {
            Activar.jugador.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotationZ;
        }

        // Si colisiona con un objeto con el tag bala destruye el objeto con el que ha colisionado,desactiva el script al que esta accediendo e inicia las corrutinas
        // Si no colisiona con un objeto con el tag bala congela la rotacion en z del rigidbody al que esta accediendo y hace que las variables de velocidad del script al que esta accediendo tengan un valor

        if (choque.collider.tag == "bala")
        {
            Destroy(choque.gameObject);
            Activar.enabled = false;
            StartCoroutine(Reaparecer());
            StartCoroutine(Inmunidad());
        }
        else
        {
            Activar.jugador.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotationZ;
            Activar.velocidadLat = 0.3f;
            Activar.RotacionLat = 60;
            Activar.velocidadFront = 25;
        }
    }

    void OnTriggerEnter(Collider other)
    {

        // Triggers que activan condiciones por medio de un tag

        // Si colisiona con un objeto con el tag ActivarCilindro aparece el objeto Cilindro en una posiocion y rotacion de referencia asigando dede el inspector en unity, al objeto Cilindro le a�ade una fuerza hacia abajo y lo destruye despues de un tiempo(duracion)

        if (other.GetComponent<Collider>().tag == "ActivarCilindro")
        {
            GameObject AparecerCilindro = Instantiate(Cilindro, SpawnRefCilindro.transform.position, SpawnRefCilindro.transform.rotation);
            AparecerCilindro.GetComponent<Rigidbody>().AddForce(Vector3.up * impulsoAbajo);
            Destroy(AparecerCilindro, duracion);
        }

        // Si colisiona con un objeto con el tag ActivarCilindroInv aparece el objeto Cilindro en una posiocion y rotacion de referencia asigando dede el inspector en unity, al objeto Cilindro le a�ade una fuerza hacia abajo y lo destruye despues de un tiempo(duracion)

        if (other.GetComponent<Collider>().tag == "ActivarCilindroInv")
        {
            GameObject AparecerCilindroInv = Instantiate(Cilindro, SpawnRefCilindroInv.transform.position, SpawnRefCilindroInv.transform.rotation);
            AparecerCilindroInv.GetComponent<Rigidbody>().AddForce(Vector3.up * impulsoAbajo);
            Destroy(AparecerCilindroInv, duracion);
        }

        // Si colisiona con un objeto con el tag meta desactiva la camara del jugador
        // desactiva la camara del jugador
        // activa la camara de meta e incia su animaci�n
        // activa el menu de fin de nivel
        // desactiva el script al que esta accediendo
        // las variables de velocidad del script al que esta accediendo les da un valor
        // la rotacion del jugador se vuelve cero
        // el pitch del audio del jugador pasa a cero
        // la camara del jugador mira al jugador
        // el foreach hara aparecer un efecto de confeti en los spawns asigandos desde el inspector de unity por medio de emptys

        // Sin no colisiona con un objeto con el tag meta desactivara las animaciones y camaras de la meta y activara la camara del jugador

        if (other.GetComponent<Collider>().tag == "meta")
        {
            camaraJugador.enabled = false;
            CamaraMeta.enabled = true;
            cinematica.enabled = true;
            cinematica.SetBool("llegar", true);
            menu.SetActive(true);
            Activar.enabled = false;
            Activar.velocidadFront = 0f;
            Activar.velocidadLat = 0f;
            Activar.jugador.transform.Rotate(0, 0, 0);
            Activar.acelerar.pitch = 0;
            camaraJugador.transform.LookAt(objetivoCam);
            foreach (GameObject aparecer in spawnerConfeti)
            {
                Instantiate(confeti, aparecer.transform.position, aparecer.transform.rotation);
            }
        }
        else
        {
            CamaraMeta.enabled = false;
            camaraJugador.enabled = true;
            cinematica.enabled = false;
            cinematica.SetBool("llegar", false);
        }

        // Si colisiona con un objeto con el tag impulso reproducira un sonido, aumentara la velocidad maxima del jugador y le a�adira una fuerza hacia adelante al jugador
        // Si no colisiona con un objeto con el tag impulso la velocidad maxima del jugador tendra un valor

        if (other.GetComponent<Collider>().tag == "impulso")
        {
            AudioManager.instanciar.ReproducirSFX(impulso);
            Activar.velocidadMaxFront = Activar.velocidadMaxFront * 1.25f;
            Activar.jugador.GetComponent<Rigidbody>().AddForce(Vector3.forward * (Activar.velocidadMaxFront + Activar.impulsoFront));
        }
        else
        {
            Activar.velocidadMaxFront = 50f;
        }

        // Si colisiona con un objeto con el tag laser reproducira un sonido e inciara una corrutina

        if (other.GetComponent<Collider>().tag == "laser")
        {
            AudioManager.instanciar.ReproducirSFX(laser);
            StartCoroutine(Reaparecer());
        }

        // Si colisiona con un objeto con el tag moneda reproducira un sonido, sumara una unidad al contador de monedas y destruira la moneda

        if (other.GetComponent<Collider>().tag == "moneda")
        {
            AudioManager.instanciar.ReproducirSFX(coin);
            contarMonedas.contadorMonedas += 1;
            Destroy(other.gameObject);
        }
    }

    void Seguir()
    {
        // La camara asignada al jugador sigue al jugador con una separaci�n

        camaraJugador.transform.position = objetivoCam.position + separacionCam;
        camaraJugador.transform.LookAt(objetivoCam);
    }


    public IEnumerator Reaparecer()
    {
        // Corrutina encargada de la reaparicion del jugador

        // Cuando se incia la corrutina:
        // Crear� un punto de reaparicion en la posicion del jugador mas una separacion en el eje -z de dicha posicion por medio de un empty asigando desde el inspector de unity, la rotacion que usa es la del empty y no la del jugador
        // Desactivara el script de movimiento del jugador al que esta accediendo
        // La velocidad lateral del jugador sera de cero
        // Se activa un retardo
        // Activara el script de movimiento del jugador al que esta accediendo
        // La posici�n y rotaci�n del jugador seran iguales a las del putno de reaparici�n
        // La velocidad del jugador vuelve a ser la asiganda antes de inciar la corrutina
        // A�ade una fuerza hacia adelante para el jugador

        GameObject Crear = Instantiate(respawn, Activar.jugador.transform.position + Activar.separacionRespawn, Activar.PlayerRespawnRef.transform.rotation);
        Activar.enabled = false;
        Activar.velocidadLat = Activar.velocidadLat + 0;
        yield return new WaitForSeconds(reaparicion);
        Activar.enabled = true;
        Activar.jugador.transform.position = Crear.transform.position;
        Activar.jugador.transform.rotation = Crear.transform.rotation;
        Activar.velocidadLat = 0 + Activar.velocidadLat;
        Activar.jugador.GetComponent<Rigidbody>().AddForce(Vector3.forward * (Activar.velocidadMaxFront + Activar.impulsoFront));
    }

    IEnumerator ReaparecerIzq()
    {
        // Corrutina encargada de la reaparicion del jugador del lado izquierdo de la pista

        // Cuando se incia la corrutina:
        // Crear� un punto de reaparicion en la posicion del jugador mas una separacion en el eje -x de dicha posicion por medio de un empty asigando desde el inspector de unity, la rotacion que usa es la del empty y no la del jugador
        // Desactivara el script de movimiento del jugador al que esta accediendo
        // La velocidad lateral del jugador sera de cero
        // Se activa un retardo
        // Activara el script de movimiento del jugador al que esta accediendo
        // La posici�n y rotaci�n del jugador seran iguales a las del putno de reaparici�n
        // La velocidad del jugador vuelve a ser la asiganda antes de inciar la corrutina
        // A�ade una fuerza hacia adelante para el jugador

        GameObject Crear = Instantiate(respawn, Activar.jugador.transform.position + Activar.separacionRespawnIzq, Activar.PlayerRespawnRef.transform.rotation);
        Activar.enabled = false;
        Activar.velocidadLat = Activar.velocidadLat + 0;
        yield return new WaitForSeconds(reaparicion);
        Activar.enabled = true;
        Activar.jugador.transform.position = Crear.transform.position;
        Activar.jugador.transform.rotation = Crear.transform.rotation;
        Activar.velocidadLat = 0 + Activar.velocidadLat;
        Activar.jugador.GetComponent<Rigidbody>().AddForce(Vector3.forward * (Activar.velocidadMaxFront + Activar.impulsoFront));
    }

    IEnumerator ReaparecerDer()
    {
        // Corrutina encargada de la reaparicion del jugador del lado derecho de la pista

        // Cuando se incia la corrutina:
        // Crear� un punto de reaparicion en la posicion del jugador mas una separacion en el eje x de dicha posicion por medio de un empty asigando desde el inspector de unity, la rotacion que usa es la del empty y no la del jugador
        // Desactivara el script de movimiento del jugador al que esta accediendo
        // La velocidad lateral del jugador sera de cero
        // Se activa un retardo
        // Activara el script de movimiento del jugador al que esta accediendo
        // La posici�n y rotaci�n del jugador seran iguales a las del putno de reaparici�n
        // La velocidad del jugador vuelve a ser la asiganda antes de inciar la corrutina
        // A�ade una fuerza hacia adelante para el jugador

        GameObject Crear = Instantiate(respawn, Activar.jugador.transform.position + Activar.separacionRespawnDer, Activar.PlayerRespawnRef.transform.rotation);
        Activar.enabled = false;
        Activar.velocidadLat = Activar.velocidadLat + 0;
        yield return new WaitForSeconds(reaparicion);
        Activar.enabled = true;
        Activar.jugador.transform.position = Crear.transform.position;
        Activar.jugador.transform.rotation = Crear.transform.rotation;
        Activar.velocidadLat = 0 + Activar.velocidadLat;
        Activar.jugador.GetComponent<Rigidbody>().AddForce(Vector3.forward * (Activar.velocidadMaxFront + Activar.impulsoFront));
    }

    IEnumerator Inmunidad()
    {
        // Corrutina encargada de desactivar la torreta

        // Si no hay colision desactivara el script de la torreta
        // Inicara un retardo
        // El bool de colision sera verdadero
        // Activara el script de la torreta
        
        if (colision == false)
        {
            Apagar.enabled = false;
            yield return new WaitForSeconds(ApagarTorreta);
            colision = true;
            Apagar.enabled = true;
        }
    }
}