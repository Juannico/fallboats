using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class MenuPrincipal : MonoBehaviour
{
    // Las siguientes funciones solo son para propositos esteticos en el inspector de unity y no afectan la funci�n del c�digo en si

    // Space(espaciado en el inspector)
    // Header(titulo en el inspector)
    // Tooltip(texto al dejar el cursor sobre una variable en el inspector)

    [Space(10)]
    [Header("Variables")]

    [Tooltip("El menu principal que se usara")]
    public GameObject menuPrincipal;
    [Tooltip("El menu de opciones que se usara")]
    public GameObject menuOpciones;

    void Start()
    {
        // El menu de opciones esta desactivado por defecto
        
        menuOpciones.SetActive(false);
    }
    public void Jugar()
    {
        // Funci�n encargada de cargar el primer nivel

        SceneManager.LoadScene(1);
    }
    public void Opciones()
    {
        // Funci�n encargada de activar las opciones del juego

        menuPrincipal.SetActive(false);
        menuOpciones.SetActive(true);
    }
    public void Volver()
    {
        // Funci�n encargada de volver al menu principal

        menuOpciones.SetActive(false);
        menuPrincipal.SetActive(true);
    }
    public void Salir()
    {
        // Funci�n encargada de salir del juego

        Application.Quit();
    }
}
