using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class MenuPlayer : MonoBehaviour
{
    // Las siguientes funciones solo son para propositos esteticos en el inspector de unity y no afectan la funci�n del c�digo en si

    // Space(espaciado en el inspector)
    // Header(titulo en el inspector)
    // Tooltip(texto al dejar el cursor sobre una variable en el inspector)

    [Space(10)]
    [Header("Variables")]

    [Tooltip("El menu de pausa que se usara")]
    public GameObject menuPausa;
    [Tooltip("El menu de salida que se usara")]
    public GameObject menuSalir;
    [Tooltip("Enteros que cuentan monedas(numero) y que muestran el resultado en un TextMeshPro(contadorMonedas)")]
    public int numero, contadorMonedas;
    [Tooltip("Textos de TextMeshPro que muestra el nivel en el que esta el jugador(Nivel) y que muestra la cantidad de monedas obtenidas(Monedas)")]
    public TMP_Text Nivel, Monedas;

    private void Start()
    {
        // Menus desactivados por defecto
        
        Monedas.GetComponent<TMP_Text>();
        menuPausa.SetActive(false);
        menuSalir.SetActive(false);
        CambioNivel();
    }

    void Update()
    {

    }

    public void CambioNivel()
    {
        // Funci�n encargada del cambio de nivel

        // Si cambio es verdadero el texto que indica el nivel suma una unidad

        bool cambio;
        cambio = true;
        if (cambio)
        {
            Nivel.text = numero.ToString();
            numero += 1;
        }
        else
        {
            cambio = false;
        }
    }
    public void Reiniciar(string reinicio)
    {
        // Funci�n encargada de reiniciar el nivel

        // El switch se encarga de gestionar que nivel reiniciar dependiendo de en que nivel esta el jugador
        
        Time.timeScale = 1;
        switch (reinicio)
        {
            case "uno":
                SceneManager.LoadScene(1);
                break;

            case "dos":
                SceneManager.LoadScene(2);
                break;

            case "tres":
                SceneManager.LoadScene(3);
                break;

            case "cuatro":
                SceneManager.LoadScene(4);
                break;

            case "cinco":
                SceneManager.LoadScene(5);
                break;
        }
    }

    public void SalirNivel()
    {
        // Funci�n encargada de salir del nivel

        // Por defecto el menu de pausa esta desactivado y el menu de salir esta activado

        menuPausa.SetActive(false);
        menuSalir.SetActive(true);
    }

    public void Si()
    {
        // Funci�n encargada de volver al menu principal
        
        // Carga la escena del menu principal

        Time.timeScale = 1;
        SceneManager.LoadScene(0);
    }

    public void No()
    {
        // Funci�n encargada de volver al menu de pausa

        // Por defecto el menu de pausa esta activado y el menu de salir esta desactivado

        menuSalir.SetActive(false);
        menuPausa.SetActive(true);
    }

    public void Pausar()
    {
        // Funci�n encargada de pausar el juego

        // El tiempo se congela y se activa el menu de pausa

        Time.timeScale = 0;
        menuPausa.SetActive(true);
    }

    public void Continuar()
    {
        // Funci�n encargada de continuar el juego

        // El tiempo avanza a velocidad normal y se desactiva el menu de pausa

        Time.timeScale = 1;
        menuPausa.SetActive(false);
    }

    public void SiguienteNivel(string nivel)
    {
        // Funci�n encargada de avanzar al siguiente nivel

        // El switch se encarga de gestionar a que nivel avanzar dependiendo de en que nivel esta el jugador

        switch (nivel)
        {
            case "uno":
                SceneManager.LoadScene(1);
                CambioNivel();

                break;

            case "dos":
                SceneManager.LoadScene(2);
                CambioNivel();
                break;

            case "tres":
                SceneManager.LoadScene(3);
                CambioNivel();
                break;

            case "cuatro":
                SceneManager.LoadScene(4);
                CambioNivel();
                break;

            case "cinco":
                SceneManager.LoadScene(5);
                CambioNivel();
                break;
            case "random":
                SceneManager.LoadScene(Random.Range(1, 5));
                CambioNivel();
                break;
        }
    }
       
}
