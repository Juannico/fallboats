using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;

public class AdsManager : MonoBehaviour
{
    #region Verificar SO

    // Verifica el sistema operativo donde se ejecuta la APK

#if UNITY_IOS
    private string gameId = "4744093";
#elif UNITY_ANDROID
    private string gameId = "4744092";
#endif

    #endregion

    // booleano que coordian el modo de prueba
    bool testMode = true;

    void Start()
    {
        // inicia los ads
        
        Advertisement.Initialize(gameId, testMode);
    }

    void Update()
    {
        
    }

    public void MostrarAd()
    {
        // Funci�n encargada de mostrar los ads

        // Si los ads estan listos los mostrara
        
        if (Advertisement.IsReady())
        {
            Advertisement.Show();
        }
        else
        {
            print("el ad no se esta mostrando");
        }
    }
}
