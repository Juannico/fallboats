﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ControlBots : MonoBehaviour
{
    // Las siguientes funciones solo son para propositos esteticos en el inspector de unity y no afectan la función del código en si

    // Space(espaciado en el inspector)
    // Header(titulo en el inspector)
    // Tooltip(texto al dejar el cursor sobre una variable en el inspector)

    [Space(10)]
    [Header("Variables de los bots")]

    [Tooltip("El objeto al que va a seguir el bot")]
    public Transform objetivo;
    [Tooltip("El navmesh con el que el bot se movera hasta su objetivo")]
    public NavMeshAgent ruta;
    [Tooltip("El bot que se usara")]
    public GameObject bot;

    void Update()
    {
        // El destino del navmesh es igual a la posición del objetivo

        ruta.destination = objetivo.position;
    }

    void OnCollisionEnter(Collision collision)
    {
        // Colisiones que activan condiciones por medio de un tag

        // Si colisiona con un objeto con el tag madera desactiva el navmesh del bot y destruye el objeto con el que colisiona
        // Si no colisiona con un objeto con el tag madera activa el navmesh del bot

        if (collision.collider.tag == "madera")
        {
            ruta.enabled = false;
            Destroy(collision.gameObject);
        }
        else
        {
            ruta.enabled = true;
        }

        // Si colisiona con un objeto con el tag Player desactiva el navmesh del bot
        // Si no colisiona con un objeto con el tag Player activa el navmesh del bot

        if (collision.collider.tag == "Player")
        {
            ruta.enabled = false;
        }
        else
        {
            ruta.enabled = true;
        }
    }

    void OnCollisionStay(Collision collision)
    {
        // Colisiones continuas que activan condiciones por medio de un tag

        // Si colisiona con un objeto con el tag madera desactiva el navmesh del bot y destruye el objeto con el que colisiona
        // Si no colisiona con un objeto con el tag madera activa el navmesh del bot

        if (collision.collider.tag == "concreto")
        {
            ruta.enabled = false;
            Destroy(collision.gameObject);
        }
        else
        {
            ruta.enabled = true;
        }
    }
}
