using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    // Las siguientes funciones solo son para propositos esteticos en el inspector de unity y no afectan la funci�n del c�digo en si

    // Space(espaciado en el inspector)
    // Header(titulo en el inspector)
    // Tooltip(texto al dejar el cursor sobre una variable en el inspector)

    [Space(10)]
    [Header("Variables")]

    [Tooltip("clase a la que esta accediendo el script")]
    public static AudioManager instanciar;
    [Tooltip("Audiosource al que accede el script")]
    public AudioSource Sonido, SFX, MaderaRota, Nitro, ConcretoRoto, laser, explosion, moneda;
    private void Awake()
    {
        // Condicion para coordinar los emptys donde apareceran y reproduciran los sonidos
        
        if (instanciar == null)
        {
            instanciar = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void ReproducirSFX(AudioClip pista)
    {
        // Funci�n encargada de reproducir los sonidos asignados una sola vez

        SFX.PlayOneShot(pista);
        MaderaRota.PlayOneShot(pista);
        Nitro.PlayOneShot(pista);
        ConcretoRoto.PlayOneShot(pista);
        laser.PlayOneShot(pista);
        explosion.PlayOneShot(pista);
        moneda.PlayOneShot(pista);
    }

    public void ReproducirMusica(AudioClip musica)
    {
        // Funci�n encargada de reprudir los sonidos asignados

        Sonido.clip = musica;
        Sonido.Play();
    }
}
